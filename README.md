## Introduction

This is my final project to finish my Computer Science degree. It consists of an app that can recogize physical exercises in real time using a Kinect V2 sensor. This repository contains every step of the development, from aquiring the data to process, to choosing the ML/DL model to classify the exercises and using it in an app that recognizes in real time.

## Technologies Used

#### Hardware:
* Kinect V2 sensor developed by Microsoft (discontinued in 2017)

#### Software: 
* Programing Languages: 
    * [C# 7.3](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7) 
    * [Pyhton 3.8.10](https://www.python.org/downloads/release/python-3810/)

<br>

* C# Libraries and Frameworks:
    * [.NET Framework 4.8](https://dotnet.microsoft.com/download/dotnet-framework/net48)
    * Kinect API (comes with [Kinect for Windows SDK](https://www.microsoft.com/en-us/download/details.aspx?id=44561))
    * [Kinect.Tools API](https://www.nuget.org/packages/Microsoft.Kinect.Tools.x64/)
    * [CsvHelper](https://www.nuget.org/packages/CsvHelper/)
    * [ONNX Runtime](https://onnxruntime.ai/docs/get-started/with-csharp.html)

<br>

* Python Libraries and Frameworks:
    * [Numpy 1.19.5](https://pypi.org/project/numpy/1.19.5/)
    * [Pandas 1.3.0](https://pypi.org/project/pandas/1.3.0/)
    * [SciPy 1.7.0](https://pypi.org/project/scipy/1.7.0/)
    * [Scikit-learn 0.24.2](https://pypi.org/project/scikit-learn/0.24.2/)
    * [TensorFlow 2.5.0](https://pypi.org/project/tensorflow/2.5.0/)
    * [skl2onnx 1.9.2](https://pypi.org/project/skl2onnx/1.9.2/)

<br>

* Kinect Studio App

## Capture Quality Tests
Quality tests were performed to the skeleton/body capturing capabilities of the Kinect V2 sensor, to make sure in what conditions the subject's sekeleton is captured the best. The quality of capture was evaluated from 0 to 7 in which 0 means that the sensor didn't capture the subject's skeleton in any instant and 7 means that the subject's skeleton was captured perfectly performing complex movements in every instant of the capture. The following table shows the evaluations:

![Quality Tests Table](ReadmeComponents/qtests.png?raw=true)

In conclusion, the best place to put the sensor is at a distance between 200 cm and 300 cm of the athlete, with 0º of tilt angle, 100 cm of height relatively to the ground and in front of the athlete/model.

## Aquired Data
Through the Kinect API you can collect, from the subject's simulated skeleton, the x, y and z coordinates in space of 25 joints, relatively to the sensor. Firstly was created a script using the Kinect.Tools API to run the recorded video samples of the exercises, and with the Kinect API the joint coordinates were captured and then saved in a csv file to train the classification model. A problem was noticed in classification, training a model with the 3D coordinates of joints in space. The problem was that every person has different distances between joints, which means that it will make very hard to the classifier find patterns between coordinates changes of the joints to recognize which exercise is being performed. The same joint will never be in the same place in space and will never change position the same way between two different people, or even the same person in different instances. So the angles formed by three joints were calculated and saved in the csv files. Fourteen angles of the human body were chosen, to be saved from each video frame, into a csv file. This approach was taken because a body gesture is "made" of angle changes. No physical exercise can have the same angle variation as some other exercise, and angles are not influenced by body size. The script that captures the 14 angles is the onde on the BFE_Angles directory. The following formula was used to calculate the angles between three points in space: 

<img src="ReadmeComponents/angform.PNG" alt="Formula" width="320"/>

The 14 angles were chosen because they seemed to be the most relevante to the majority of physical exercises a person can perform and are the following:

![Body Angles](ReadmeComponents/angs.png?raw=true)

The data is saved in the csv files in the following format:

![CSV Example](ReadmeComponents/csvExemp.PNG)

## Data Processing

To use ML/DL models, the data must first be processed and normalized. Because models only accept arrays with fixed size dimensions as its feature set, the approach here was to "stretch" the data of the shortest sample (in amount of data it contains) to the size of the biggest sample size. Using the functions zoom() and CubicSpline() from the SciPy library this was possible in a relatively simple way. Firstly is used zoom() in each array of idexes of the data of a given sample and increased its size, using the ratio between the quantity of data of the biggest sample and the quantity of the current sample being processed. What this does is create a new array with the new size that contains all the idex values given and new values in between those that were given. With the data related with the angles are made 14 cubic splines of each angle type. Using CubicSpline() is obtained functions that pass through each data "point" given, because CubicSpline() makes an interpolation of the data given. Finally is introduced each index in each interpolation to obtain the new values between the given data values, then creating a "streched" version of the sample that is being processed. The data processing script is called DataPreProcessing.py and is on the PythonScripts directory. The following to graphs show a before and after of the processing of a single angle type.

Before processing:
![Before Processing](ReadmeComponents/unproc_graph2.png)

After processing:
![After Processing](ReadmeComponents/proc_graph2.png)

## ML/DL Models

Two models are used in this project. One is available on Scikit-learn called RandomForestClassifier and another was created using TensorFlow. RandomForestClassifier is used because it's a generally good classification model and can be applied to sequencial data like video. During testing, its success was noticed on recognizing gestures accuratly. The script with the RandomForestClassifier is called RFClassifier.py and is on the PythonScripts directory.

The TensorFlow model developed was a CNN because the data is sequencial, and is constituted by three layers. Two layers of LSTM nodes with 14 nodes each and on the last dense layer with seven nodes (because it was the number of gestures to be recognised in the development phase). The data is inserted in the model during training in batches of 14 because it is the number of types of angles collected. The script with the TensorFlow CNN is called TF_DeepNN.py and is on the PythonScripts directory. The following graph shows a summurised version of the network:

![TensorFlow CNN](ReadmeComponents/dnn_tf.png)

Because the amount data of the dataset wasn't great during tests, the recognition results showed that this model was unsuccessful to recognize gestures, so it was discarded from the final version of the application of this project.

## App

For the interface of the app it was used a sample code given by Microsoft, available to download in the Kinect SDK Browser app. On top of this sample code the model was applied. Because the RandomForestClassifier was chosen and there is not a version of Scikit-learn available for C#, the model was converted to an ONNX model (the script to make this conversion is called ToONNX.py and is stored in the PythonScripts directory). While the application is running the skeleton of the subject exposed to the camera is collected, the angles are calculated and the introduced in the model to return what is the subject performing, in real time. The app is on the Savitar-Kinect directory. You can watch the app working in [this Youtube video sample](https://www.youtube.com/watch?v=-ggAhxp7Ssg).
