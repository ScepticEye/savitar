import numpy as np
import pandas as pd
from scipy.interpolate import CubicSpline
from scipy.ndimage import zoom


# Returns the currently open array size
def get_array_size(current_file, step):
    # Reads a csv as a DataFrame
    csv_file = pd.read_csv(current_file)

    # Creates a numpy array with the range defined by the file size with values with a given step
    single_joint = np.arange(0, csv_file.shape[0], step)
    return len(single_joint)


# Returns the array from the file enlarged to the size of the biggest array in the dataset and interpolates it's data
def get_zoomed(current_file, max_length, step):
    # Reads a csv as a DataFrame but get only the values
    csv_file = pd.read_csv(current_file).values
    # Size of a single angle type array (in integer)
    int_ang_size = len(np.arange(0, csv_file.shape[0], step))
    # Converts to a numpy double the integer value of the single angle type array
    single_ang_size = np.double(int_ang_size)
    # Creates an array with the indexes in doubles
    index_arr = np.arange(np.double(0), single_ang_size)

    # print('Original indexes array = ', index_arr.shape)

    # List that saves the cubic splines from each angle type
    splines_list = []

    # Cycles through each angle type
    for x in range(step):
        # Array that holds the indexes of a type of angle
        ang_index = np.arange(x, csv_file.shape[0], step)
        # Array that holds a single angle type values taken from the original array
        ang_val = np.ravel(np.take(csv_file, ang_index))
        # Creates a cubic spline from the indexes and the angles values
        cubic_spline = CubicSpline(index_arr, ang_val)
        # Appends the spline to the cubic splines list
        splines_list.append(cubic_spline)

    # Calculates the number to which we need to multiply the short array to get to the size of the largest array
    amplify = max_length / int_ang_size

    # Using the ndimage.zoom from the scipy library the array is enlarged and interpolated according to the multiplier
    # giving us the new indexes to use later
    index_zoomed = zoom(index_arr, amplify)

    # print('NEW array of indexes = ', index_zoomed.shape)

    # Array that holds the values of all the angles
    final_arr = []

    for y in range(max_length):
        # index to get value from the splines
        index = index_zoomed[y]

        # Cycles through the types of angles
        for k in range(step):
            # Gets a spline from the splines list
            cs = splines_list[k]
            # A value from the spline is appended to the array of values
            final_arr.append(cs(index))

    # Turns the array of angles into a 1D array
    np.ravel(final_arr)
    # print('Shape of final array = ', len(final_arr))
    # print('----')

    return np.asarray(final_arr)


# Returns the array from the file enlarged to the size of the biggest array in the dataset and interpolates it's data
# in a 3 dimensional shape
def get_3D_zoomed(current_file, max_length, step):
    # Reads a csv as a DataFrame but get only the values
    csv_file = pd.read_csv(current_file).values
    # Size of a single angle type array (in integer)
    int_ang_size = len(np.arange(0, csv_file.shape[0], step))
    # Converts to a numpy double the integer value of the single angle type array
    single_ang_size = np.double(int_ang_size)
    # Creates an array with the indexes in doubles
    index_arr = np.arange(np.double(0), single_ang_size)

    # List that saves the cubic splines from each angle type
    splines_list = []

    # Cycles through each angle type
    for x in range(step):
        # Array that holds the indexes of a type of angle
        ang_index = np.arange(x, csv_file.shape[0], step)
        # Array that holds a single angle type values taken from the original array
        ang_val = np.ravel(np.take(csv_file, ang_index))
        # Creates a cubic spline from the indexes and the angles values
        cubic_spline = CubicSpline(index_arr, ang_val)
        # Appends the spline to the cubic splines list
        splines_list.append(cubic_spline)

    # Calculates the number to which we need to multiply the short array to get to the size of the largest array
    amplify = max_length / int_ang_size

    # Using the ndimage.zoom from the scipy library the array is enlarged and interpolated according to the multiplier
    # giving us the new indexes to use later
    index_zoomed = zoom(index_arr, amplify)

    # print('NEW array of indexes = ', index_zoomed.shape)

    # Array that holds the values of all the angles
    final_arr = []

    for y in range(max_length):
        # index to get value from the splines
        index = index_zoomed[y]

        frame_arr = []

        # Cycles through the types of angles
        for k in range(step):
            # Gets a spline from the splines list
            cs = splines_list[k]
            # A value from the spline is appended to the array of values
            frame_arr.append(cs(index))

        final_arr.append(frame_arr)

    # Turns the array of angles into a 1D array
    # np.ravel(final_arr)
    print('Shape of final array = ', len(final_arr))
    # print('----')

    return np.asarray(final_arr)
