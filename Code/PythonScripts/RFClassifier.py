import os
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import Pipeline
import pickle
from sklearn.calibration import CalibratedClassifierCV
import DataPreProcess as dpp
import ToONNX

dsPath = "/home/owner/Documents/Processed_Dataset/"
labels = os.listdir(dsPath)
max_length = 0
# Pipeline created with the preprocessing stages and the classifier at the end
pipe_line = Pipeline(
    [('Normalizer', Normalizer()), ('RFC', RandomForestClassifier(n_jobs=-1))])
samples_list = []
y_labels = []
step = 14

# Iterates through the directories and files finding the largest array and saving it's size value in a global variable
for label in labels:
    current_dir = dsPath + label
    files = os.listdir(current_dir)

    for file in files:
        currentFile = current_dir + "/" + file

        # The function that returns the array size is called
        f = dpp.get_array_size(currentFile, step)
        if f > max_length:
            max_length = f

# print('Size of the biggest sample = ', max_length)

# Iterates through the directories and files to create the array of samples and features to fit in the scikit learn
# algorithms
for label in labels:
    current_dir = dsPath + label
    files = os.listdir(current_dir)

    for file in files:
        currentFile = current_dir + "/" + file
        # print(currentFile)
        # The function that returns the enlarged and interpolated array is called
        f = dpp.get_zoomed(currentFile, max_length, step)

        # Appends the array gotten from the function to a list
        samples_list.append(f)

        # Appends the labels to the list that holds class labels for classification
        y_labels.append(label)

# Converts the list into a numpy array
arr = np.array(samples_list, dtype=float)
# Reshapes the numpy array to the way that allows to get the samples and features correctly
arr_shaped = arr.reshape((len(arr), -1))

# Turns the 2D array of labels into a 1D array
np.ravel(y_labels)

print('Samples array shape = ', arr_shaped.shape)
print('Labels array size = ', len(y_labels))

# Fits the dataset samples and class labels into the pipeline
pipe_line.fit(arr_shaped, y_labels)

# Path to the test classification test file
tempCsv = '/home/owner/Documents/Comparation_Data/1.csv'

# The function that returns the enlarged and interpolated array is called to get the test array
pred = dpp.get_zoomed(tempCsv, max_length, step)

# Reshape the array to the shape needed to get the prediction
pred_reshaped = pred.reshape((1, -1))

print('-----')

# Prints the prediction stats using the pipeline
print('Predicted Label = ', pipe_line.predict(pred_reshaped)[0])
print('Accuracy Scores = ', pipe_line.predict_proba(pred_reshaped)[0])

# save trained model
pickle.dump(pipe_line, open('/home/owner/Documents/savitar/Code/MlScripts/SavedModels/trained_model.pickle', 'wb'))

print(arr.shape[1])

ToONNX.make_onnx(pipe_line, arr.shape[1])
