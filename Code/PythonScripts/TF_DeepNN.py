import tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, LSTM
import os
import DataPreProcess as dpp
import numpy as np

dsPath = "/home/owner/Documents/Processed_Dataset/"
labels = os.listdir(dsPath)
max_length = 0
samples_list = []
y_labels = []
step = 14

# Iterates through the directories and files finding the largest array and saving it's size value in a global variable
for label in labels:
    current_dir = dsPath + label
    files = os.listdir(current_dir)

    for file in files:
        currentFile = current_dir + "/" + file

        # The function that returns the array size is called
        f = dpp.get_array_size(currentFile, step)
        if f > max_length:
            max_length = f

print('Size of the biggest sample = ', max_length)

# Iterates through the directories and files to create the array of samples and features to fit in the scikit learn
# algorithms
for index, label in enumerate(labels):
    current_dir = dsPath + label
    files = os.listdir(current_dir)

    for file in files:
        currentFile = current_dir + "/" + file
        print(currentFile)
        # The function that returns the enlarged and interpolated array is called
        f = dpp.get_3D_zoomed(currentFile, max_length, step)

        # Appends the array gotten from the function to a list
        samples_list.append(f)

        # Appends the labels to the list that holds class labels for classification
        y_labels.append(index)

# Converts the list into a numpy array
arr = np.array(samples_list, dtype=float)
arr = arr / 180.0
# Reshapes the numpy array to the way that allows to get the samples and features correctly
# arr_shaped = arr.reshape((len(arr), -1))

# Turns the 2D array of labels into a 1D np.array
y_labels = np.asarray(y_labels)
np.ravel(y_labels)
# Turns labels array into a one hot encoded array, good for classification
y_labels = tf.one_hot(y_labels, 7)

print('Samples array shape = ', arr.shape)
print('Labels array size = ', y_labels.shape)

# An RNN model is created
model = Sequential([
    LSTM(step, activation='tanh', recurrent_activation='sigmoid', recurrent_dropout=0, unroll=False, use_bias=True,
         return_sequences=True),
    LSTM(step, activation='tanh', recurrent_activation='sigmoid', recurrent_dropout=0, unroll=False, use_bias=True,
         return_sequences=False),
    Dense(len(labels), activation='softmax')
])

# The model is compiled
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Fits the dataset samples and class labels
model.fit(arr, y_labels, epochs=500)

print(model.summary())

print('----- an exit(0) is in the code')
