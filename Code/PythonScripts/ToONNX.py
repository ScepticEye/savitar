from skl2onnx.common.data_types import FloatTensorType
from skl2onnx import convert_sklearn


# Converts the scikit-learn model to an onnx model and saves into a file
def make_onnx(model, second_dim):
    # defining the input type and size of the input
    initial_type = [('float_input', FloatTensorType([second_dim]))]

    # converting the scikit-learn model to an onnx
    onnx = convert_sklearn(model, initial_types=initial_type)

    # saves the onnx model into a file
    with open("/home/owner/Documents/savitar/Code/MlScripts/SavedModels/onnx_model.onnx", "wb") as file:
        file.write(onnx.SerializeToString())
