﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//
// Modified by João Afonso Troncão Fragoso to fit the Savitar-Kinect project's purpose
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Microsoft.Kinect;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;

namespace Microsoft.Samples.Kinect.BodyBasics
{
    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<Pen> bodyColors;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        // List of lists of angles
        private List<List<float>> angleList = new List<List<float>>();

        // Max list size
        private int maxAngListSize = 134;

        // Frame counter
        private int framesPassed = 0;

        // Variable that saves the onnx model
        private InferenceSession session = new InferenceSession(@"C:\Users\Owner\Documents\savitar\Code\MlScripts\SavedModels\onnx_model.onnx");


        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // one sensor is currently supported
            this.kinectSensor = KinectSensor.GetDefault();

            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            // get the depth (display) extents
            FrameDescription frameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;

            // get size of joint space
            this.displayWidth = frameDescription.Width;
            this.displayHeight = frameDescription.Height;

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // a bone defined as a line between two joints
            this.bones = new List<Tuple<JointType, JointType>>();

            // Torso
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

            // Right Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

            // Left Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

            // Right Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

            // Left Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));

            // populate body colors, one for each BodyIndex
            this.bodyColors = new List<Pen>();

            this.bodyColors.Add(new Pen(Brushes.Red, 6));
            this.bodyColors.Add(new Pen(Brushes.Orange, 6));
            this.bodyColors.Add(new Pen(Brushes.Green, 6));
            this.bodyColors.Add(new Pen(Brushes.Blue, 6));
            this.bodyColors.Add(new Pen(Brushes.Indigo, 6));
            this.bodyColors.Add(new Pen(Brushes.Violet, 6));

            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                using (DrawingContext dc = this.drawingGroup.Open())
                {
                    // Draw a transparent background to set the render size
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                    int penIndex = 0;
                    foreach (Body body in this.bodies)
                    {
                        Pen drawPen = this.bodyColors[penIndex++];

                        if (body.IsTracked)
                        {
                            this.DrawClippedEdges(body, dc);

                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                            foreach (JointType jointType in joints.Keys)
                            {
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;
                                if (position.Z < 0)
                                {
                                    position.Z = InferredZPositionClamp;
                                }

                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                            }

                            this.DrawBody(joints, jointPoints, dc, drawPen);

                            /* Every body joint that is relevant to a physical exercise judgment is saved in a variable */
                            /* Not including thumb and hand tip because it doesn't seem relevant for a physical exercise judgement */
                            Joint spineShoulder = body.Joints[JointType.SpineShoulder];
                            Joint shoulderLeft = body.Joints[JointType.ShoulderLeft];
                            Joint shoulderRight = body.Joints[JointType.ShoulderRight];
                            Joint elbowLeft = body.Joints[JointType.ElbowLeft];
                            Joint elbowRight = body.Joints[JointType.ElbowRight];
                            Joint wristLeft = body.Joints[JointType.WristLeft];
                            Joint wristRight = body.Joints[JointType.WristRight];
                            Joint spineMid = body.Joints[JointType.SpineMid];
                            Joint spineBase = body.Joints[JointType.SpineBase];
                            Joint hipLeft = body.Joints[JointType.HipLeft];
                            Joint hipRight = body.Joints[JointType.HipRight];
                            Joint kneeLeft = body.Joints[JointType.KneeLeft];
                            Joint kneeRight = body.Joints[JointType.KneeRight];
                            Joint ankleLeft = body.Joints[JointType.AnkleLeft];
                            Joint ankleRight = body.Joints[JointType.AnkleRight];
                            Joint footLeft = body.Joints[JointType.FootLeft];
                            Joint footRight = body.Joints[JointType.FootRight];

                            /* Tuples of joint coordinates */
                            Tuple<float, float, float> sS = Tuple.Create(spineShoulder.Position.X, spineShoulder.Position.Y, spineShoulder.Position.Z);
                            Tuple<float, float, float> sL = Tuple.Create(shoulderLeft.Position.X, shoulderLeft.Position.Y, shoulderLeft.Position.Z);
                            Tuple<float, float, float> sR = Tuple.Create(shoulderRight.Position.X, shoulderRight.Position.Y, shoulderRight.Position.Z);
                            Tuple<float, float, float> eL = Tuple.Create(elbowLeft.Position.X, elbowLeft.Position.Y, elbowLeft.Position.Z);
                            Tuple<float, float, float> eR = Tuple.Create(elbowRight.Position.X, elbowRight.Position.Y, elbowRight.Position.Z);
                            Tuple<float, float, float> wL = Tuple.Create(wristLeft.Position.X, wristLeft.Position.Y, wristLeft.Position.Z);
                            Tuple<float, float, float> wR = Tuple.Create(wristRight.Position.X, wristRight.Position.Y, wristRight.Position.Z);
                            Tuple<float, float, float> sM = Tuple.Create(spineMid.Position.X, spineMid.Position.Y, spineMid.Position.Z);
                            Tuple<float, float, float> sB = Tuple.Create(spineBase.Position.X, spineBase.Position.Y, spineBase.Position.Z);
                            Tuple<float, float, float> hL = Tuple.Create(hipLeft.Position.X, hipLeft.Position.Y, hipLeft.Position.Z);
                            Tuple<float, float, float> hR = Tuple.Create(hipRight.Position.X, hipRight.Position.Y, hipRight.Position.Z);
                            Tuple<float, float, float> kL = Tuple.Create(kneeLeft.Position.X, kneeLeft.Position.Y, kneeLeft.Position.Z);
                            Tuple<float, float, float> kR = Tuple.Create(kneeRight.Position.X, kneeRight.Position.Y, kneeRight.Position.Z);
                            Tuple<float, float, float> aL = Tuple.Create(ankleLeft.Position.X, ankleLeft.Position.Y, ankleLeft.Position.Z);
                            Tuple<float, float, float> aR = Tuple.Create(ankleRight.Position.X, ankleRight.Position.Y, ankleRight.Position.Z);
                            Tuple<float, float, float> fL = Tuple.Create(footLeft.Position.X, footLeft.Position.Y, footLeft.Position.Z);
                            Tuple<float, float, float> fR = Tuple.Create(footRight.Position.X, footRight.Position.Y, footRight.Position.Z);

                            /* Angles */
                            float one = GetAngle(sS, sL, sR);
                            float two = GetAngle(eL, sL, wL);
                            float three = GetAngle(eR, sR, wR);
                            float four = GetAngle(sM, sS, sB);
                            float five = GetAngle(sB, sM, kL);
                            float six = GetAngle(sB, sM, kR);
                            float seven = GetAngle(sL, wL, hL);
                            float eight = GetAngle(sR, wR, hR);
                            float nine = GetAngle(kL, hL, aL);
                            float ten = GetAngle(kR, hR, aR);
                            float eleven = GetAngle(aL, kL, fL);
                            float twelve = GetAngle(aR, kR, fR);
                            float thirteen = GetAngle(sS, sM, sL);
                            float fourteen = GetAngle(sS, sM, sR);

                            List<float> frameAngList = new List<float>();
                            frameAngList.Add(one);
                            frameAngList.Add(two);
                            frameAngList.Add(three);
                            frameAngList.Add(four);
                            frameAngList.Add(five);
                            frameAngList.Add(six);
                            frameAngList.Add(seven);
                            frameAngList.Add(eight);
                            frameAngList.Add(nine);
                            frameAngList.Add(ten);
                            frameAngList.Add(eleven);
                            frameAngList.Add(twelve);
                            frameAngList.Add(thirteen);
                            frameAngList.Add(fourteen);

                            angleList.Add(frameAngList);
                            this.framesPassed++;

                            if (angleList.Count > maxAngListSize)
                            {
                                angleList.RemoveAt(0);

                                if (FramePassed(this.framesPassed))
                                {
                                    List<float> tmpList = angleList.SelectMany(x => x).ToList();

                                    // Create Tensor with the shape of the angles list array
                                    Tensor<float> tensorShape = new DenseTensor<float>(new[] { tmpList.Count });

                                    // Fill in the tensor with the angles data 
                                    for (int x = 0; x < tmpList.Count; x++)
                                    {
                                        tensorShape[x] = tmpList[x];
                                    }

                                    // A List of NamedOnnxValues is created from the tensor
                                    var input = new List<NamedOnnxValue>
                                    {
                                        NamedOnnxValue.CreateFromTensor("float_input", tensorShape)
                                    };

                                    // A prediction is made with the input angles taken in real time with the kinect sensor
                                    IDisposableReadOnlyCollection<DisposableNamedOnnxValue> pred = session.Run(inputs: input);

                                    DenseTensor<string> most_prob = (DenseTensor<string>)pred.ElementAt(0).Value;


                                    IDisposableReadOnlyCollection<DisposableNamedOnnxValue> last = (IDisposableReadOnlyCollection<DisposableNamedOnnxValue>)pred.Last().Value;

                                    DisposableNamedOnnxValue dnov = last.First();

                                    // Get a dictionary of all the probabilities 
                                    Dictionary<string, Single> keyValuePairs = (Dictionary<string, float>)dnov.Value;

                                    // Prints out the predicted and the probability
                                    foreach (KeyValuePair<String, Single> par in keyValuePairs)
                                    {
                                        if (most_prob.First() != "AberturaLateral")
                                        {
                                            if (most_prob.First() == par.Key)
                                            {
                                                Console.WriteLine("Key: " + par.Key + " Value: " + par.Value);
                                            }
                                        }
                                    }
                                }

                                

                                /*
                                if (FramePassed(framesPassed))
                                {
                                    

                                    Console.WriteLine("RESULTADO UNICO: " + most_prob.First());

                                    Console.WriteLine("Na coleção estão: " + pred.Count);

                                    foreach(DisposableNamedOnnxValue coise in pred)
                                    {
                                        Console.WriteLine("Valor: " + coise.Value);
                                    }
     
                                    Console.WriteLine("--- Count: " + last.Count);
                                    Console.WriteLine("--- First: " + last.First());
                                    Console.WriteLine("--- Last: " + last.Last());

                                    Console.WriteLine(dnov.Value.GetType());


                                    Console.WriteLine("Dictionary Count: " + keyValuePairs.Count);
                                                                        
                                }

                                */
                            }


                        }
                    }

                    // prevent drawing outside of our render area
                    this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                }
            }
        }


        // Frame counter boolean
        private bool FramePassed(int numFrames)
        {
            if (numFrames >= 30)
            {
                this.framesPassed = 0;
                return true;
            }
            return false;
        }


        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="drawingPen">specifies color to draw a specific body</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext, Pen drawingPen)
        {
            // Draw the bones
            foreach (var bone in this.bones)
            {
                this.DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
            }

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, Pen drawingPen)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = drawingPen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }


        /// <summary>
        /// Draws indicators to show which edges are clipping body data
        /// </summary>
        /// <param name="body">body to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawClippedEdges(Body body, DrawingContext drawingContext)
        {
            FrameEdges clippedEdges = body.ClippedEdges;

            if (clippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, this.displayHeight - ClipBoundsThickness, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, this.displayHeight));
            }

            if (clippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(this.displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, this.displayHeight));
            }
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }


        /* This method returns the angle between joints in space in degrees */
        private static float GetAngle(Tuple<float, float, float> center, Tuple<float, float, float> jointA, Tuple<float, float, float> jointB)
        {
            /* Variables that hold the distance between two points in space */
            float OADist = GetDistance(center, jointA);
            float OBDist = GetDistance(center, jointB);

            /* Variables that hold a tuple of a vector coordinates */
            Tuple<float, float, float> v1 = Tuple.Create(jointA.Item1 - center.Item1, jointA.Item2 - center.Item2, jointA.Item3 - center.Item3);
            Tuple<float, float, float> v2 = Tuple.Create(jointB.Item1 - center.Item1, jointB.Item2 - center.Item2, jointB.Item3 - center.Item3);

            /* Variable that holds the scalar product */
            float scalar = GetScalar(v1, v2);

            float oper = scalar / (OADist * OBDist);

            return (float)(Math.Acos(oper) * 180 / Math.PI);
        }

        /* Returns the scalar product */
        private static float GetScalar(Tuple<float, float, float> v1, Tuple<float, float, float> v2)
        {
            return (v1.Item1 * v2.Item1) + (v1.Item2 * v2.Item2) + (v1.Item3 * v2.Item3);
        }

        /* Returns the distance between two points in space */
        private static float GetDistance(Tuple<float, float, float> center, Tuple<float, float, float> joint)
        {
            float xSq = (float)Math.Pow(center.Item1 - joint.Item1, 2);
            float ySq = (float)Math.Pow(center.Item2 - joint.Item2, 2);
            float zSq = (float)Math.Pow(center.Item3 - joint.Item3, 2);

            float sum = xSq + ySq + zSq;

            return (float)Math.Sqrt(sum);
        }
    }
}
