﻿using Microsoft.Kinect.Tools;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CsvHelper;
using System.Globalization;

namespace BodyFrameExtractor
{
    /* This class exists just to hold the global variables */
    static class Global
    {
        public static KinectSensor simulSensor;
        public static BodyFrameReader bodyReader;
        public static KStudioClient client;
        public static List<JointsAngle> list;
        public static Thread tFrameCap;
    }

    /* Class that defines the objets that are going to be saved in the .csv files */
    public class JointsAngle
    {
        public double Angle { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            /* Path to where the unprocessed dataset is. */
            string unprocPath = @"C:\Users\Owner\Desktop\Dataset\";

            /* Path to where the processed dataset will be. */
            string procPath = @"C:\Users\Owner\Desktop\Processed_Dataset\";

            /* Starts and gets the default kinect sensor */
            Global.simulSensor = KinectSensor.GetDefault();

            /* Initializes a body frame reader that reads the body frames from a stream */
            Global.bodyReader = Global.simulSensor.BodyFrameSource.OpenReader();

            /* Opens the sensor */
            Global.simulSensor.Open();

            /* Creates a Kinect Studo client and connects it to the service (KinectService I belive) */
            Global.client = KStudio.CreateClient();
            Global.client.ConnectToService();

            /* A Thread is initialized to start the frame capture */
            Global.tFrameCap = new Thread(CaptureFrame);
            Global.tFrameCap.Start();

            /* Method that checks which directories existe in the unprocessed dataset and creates the directories
             * (if they don't exist already) in the processed dataset directory. Then calls another explained next. */
            FileCycler(unprocPath, procPath);
        }


        private static void FileCycler(string unprocPath, string procPath)
        {
            /* Makes an array of strings with the paths to the diferent directories 
             * in the unprocessed dataset directory */
            string[] unprocDirs = Directory.GetDirectories(unprocPath);

            foreach (string dir in unprocDirs)
            {
                /* Creates a path to a directory in the processed dataset directory, with the same name as the 
                 * one in the unprocessed dataset directory */
                string newPath = procPath + dir.Substring(dir.LastIndexOf(Path.DirectorySeparatorChar) + 1);

                /* Creates a string array of files inside the directory */
                string[] filesList = Directory.GetFiles(dir);

                if (Directory.Exists(newPath))
                {
                    /* This method copies the files from the unprocessed directory to the processed directory */
                    BuildDataset(filesList, newPath);
                }
                else
                {
                    /* Creates the directory in the processed directory */
                    Directory.CreateDirectory(newPath);

                    /* This method copies the files from the unprocessed directory to the processed directory */
                    BuildDataset(filesList, newPath);
                }
            }

            /* After all the data processing and creating the dataset, the sensor is closed */
            Global.simulSensor.Close();

            /* After all the data processing and creating the dataset, Kinect Studio disconnects 
             * from the service and disposes all objects from memory and performs a cleanup 
             * befores it is released from the memory */
            Global.client.DisconnectFromService();
            Global.client.Dispose();
        }


        /* 
         * This method gets the files from the files path difined and creates a playback. After the playback is created, the 
         * playback gets started and during it's run, the simulated kinect sensor gets the frames and processes them, then the 
         * playback is stopped e the current file is disposed. 
         */
        private static void BuildDataset(string[] filesList, string newPath)
        {
            /* Cycles through the files of a previously set directory */
            foreach (string file in filesList)
            {
                /* Get only the file name without the name extension */
                string fileName = Path.GetFileName(file);
                string[] fileNameSplit = fileName.Split('.');
                string csvFile = fileNameSplit[0];

                /* Destination file in the respective directory is defined */
                string destFile = newPath + '\\' + csvFile + ".csv";

                Console.WriteLine(file);

                /* An event file from where the streamed data will come from during playback is here defined */
                KStudioEventFile eventFile = Global.client.OpenEventFile(file);

                /* A playback is created with the previously defined event file. It's inside a using() because after playback the playback is 
                 * disposed and using() allows it to be disposed automatically after its use has ended */
                using (KStudioPlayback playback = Global.client.CreatePlayback(eventFile))
                {
                    /* A new list of the objects difined to save in the .csv files is defined */
                    Global.list = new List<JointsAngle> { };

                    /* Here is set how the playback behaves when it ends */
                    playback.EndBehavior = KStudioPlaybackEndBehavior.Stop;

                    playback.Start();
                    Console.WriteLine("Playback started!");

                    while (playback.State == KStudioPlaybackState.Playing)
                    {

                    }

                    Console.WriteLine("Playback stopped!");
                }

                /* Makes the program wait for the Thread to end what's processing */
                Global.tFrameCap.Join();

                /* This thread sleep is here to let the list of JointPositions to finish */
                Thread.Sleep(100);

                List<JointsAngle> localList = Global.list;
                string localDest = destFile;
 
                /* A Task is initialized to save the data collected from the frame in a csv file */
                Task.Run( () => {

                    Console.WriteLine(localList.Count);

                    /* StreamWriter is used to write in a file and CsvWriter is used to write on a .csv file */
                    using (StreamWriter writer = new StreamWriter(localDest))
                    using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        /* the list of JointPositions is writen onto the .csv file with .WriteRecords() */
                        csv.WriteRecords(Global.list);

                        /* The use of .Flush() is explained in an external file */
                        csv.Flush();
                    }

                    Console.WriteLine("Csv saved on: " + localDest);
                });

                /* The event file that is currently being used is released from memory */
                eventFile.Dispose();
            }

        }

        /* Initializes the BodyReader FrameArrived event to capture the frames while the stream occurs */
        private static void CaptureFrame()
        {
            Global.bodyReader.FrameArrived += BodyReader_FrameArrived;
        }

        /* 
         * Every time a frame is received this method is called. This method gets the BodyFrames from the arrived frames
         * and saves the Joint coordinates inside an array.
         */
        private static void BodyReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            /* Body frame from the frame that arrived is saved onto a variable. using() is used to dispose this variable after its use */
            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    /* This array of Body object will save the bodies obtarned from the frame */
                    Body[] bodies = new Body[bodyFrame.BodyFrameSource.BodyCount];

                    bodyFrame.GetAndRefreshBodyData(bodies);

                    foreach (Body body in bodies)
                    {
                        if (body.IsTracked)
                        {
                            /* Every body joint that is relevant to a physical exercise judgment is saved in a variable */
                            /* Not including thumb and hand tip because it doesn't seem relevant for a physical exercise judgement */
                            Joint spineShoulder = body.Joints[JointType.SpineShoulder];
                            Joint shoulderLeft = body.Joints[JointType.ShoulderLeft];
                            Joint shoulderRight = body.Joints[JointType.ShoulderRight];
                            Joint elbowLeft = body.Joints[JointType.ElbowLeft];
                            Joint elbowRight = body.Joints[JointType.ElbowRight];
                            Joint wristLeft = body.Joints[JointType.WristLeft];
                            Joint wristRight = body.Joints[JointType.WristRight];
                            Joint spineMid = body.Joints[JointType.SpineMid];
                            Joint spineBase = body.Joints[JointType.SpineBase];
                            Joint hipLeft = body.Joints[JointType.HipLeft];
                            Joint hipRight = body.Joints[JointType.HipRight];
                            Joint kneeLeft = body.Joints[JointType.KneeLeft];
                            Joint kneeRight = body.Joints[JointType.KneeRight];
                            Joint ankleLeft = body.Joints[JointType.AnkleLeft];
                            Joint ankleRight = body.Joints[JointType.AnkleRight];
                            Joint footLeft = body.Joints[JointType.FootLeft];
                            Joint footRight = body.Joints[JointType.FootRight];

                            /* Tuples of joint coordinates */
                            Tuple<float, float, float> sS = Tuple.Create(spineShoulder.Position.X, spineShoulder.Position.Y, spineShoulder.Position.Z);
                            Tuple<float, float, float> sL = Tuple.Create(shoulderLeft.Position.X, shoulderLeft.Position.Y, shoulderLeft.Position.Z);
                            Tuple<float, float, float> sR = Tuple.Create(shoulderRight.Position.X, shoulderRight.Position.Y, shoulderRight.Position.Z);
                            Tuple<float, float, float> eL = Tuple.Create(elbowLeft.Position.X, elbowLeft.Position.Y, elbowLeft.Position.Z);
                            Tuple<float, float, float> eR = Tuple.Create(elbowRight.Position.X, elbowRight.Position.Y, elbowRight.Position.Z);
                            Tuple<float, float, float> wL = Tuple.Create(wristLeft.Position.X, wristLeft.Position.Y, wristLeft.Position.Z);
                            Tuple<float, float, float> wR = Tuple.Create(wristRight.Position.X, wristRight.Position.Y, wristRight.Position.Z);
                            Tuple<float, float, float> sM = Tuple.Create(spineMid.Position.X, spineMid.Position.Y, spineMid.Position.Z);
                            Tuple<float, float, float> sB = Tuple.Create(spineBase.Position.X, spineBase.Position.Y, spineBase.Position.Z);
                            Tuple<float, float, float> hL = Tuple.Create(hipLeft.Position.X, hipLeft.Position.Y, hipLeft.Position.Z);
                            Tuple<float, float, float> hR = Tuple.Create(hipRight.Position.X, hipRight.Position.Y, hipRight.Position.Z);
                            Tuple<float, float, float> kL = Tuple.Create(kneeLeft.Position.X, kneeLeft.Position.Y, kneeLeft.Position.Z);
                            Tuple<float, float, float> kR = Tuple.Create(kneeRight.Position.X, kneeRight.Position.Y, kneeRight.Position.Z);
                            Tuple<float, float, float> aL = Tuple.Create(ankleLeft.Position.X, ankleLeft.Position.Y, ankleLeft.Position.Z);
                            Tuple<float, float, float> aR = Tuple.Create(ankleRight.Position.X, ankleRight.Position.Y, ankleRight.Position.Z);
                            Tuple<float, float, float> fL = Tuple.Create(footLeft.Position.X, footLeft.Position.Y, footLeft.Position.Z);
                            Tuple<float, float, float> fR = Tuple.Create(footRight.Position.X, footRight.Position.Y, footRight.Position.Z);

                            /* Angles */
                            double one = GetAngle(sS, sL, sR);
                            double two = GetAngle(eL, sL, wL);
                            double three = GetAngle(eR, sR, wR);
                            double four = GetAngle(sM, sS, sB);
                            double five = GetAngle(sB, sM, kL);
                            double six = GetAngle(sB, sM, kR);
                            double seven = GetAngle(sL, wL, hL);
                            double eight = GetAngle(sR, wR, hR);
                            double nine = GetAngle(kL, hL, aL);
                            double ten = GetAngle(kR, hR, aR);
                            double eleven = GetAngle(aL, kL, fL);
                            double twelve = GetAngle(aR, kR, fR);
                            double thirteen = GetAngle(sS, sM, sL);
                            double fourteen = GetAngle(sS, sM, sR);


                            /* A new JointsAngle is added to the JointsAngle list for each joint. 
                             * This includes the angle between joints*/
                            Global.list.Add(new JointsAngle { Angle = one});
                            Global.list.Add(new JointsAngle { Angle = two });
                            Global.list.Add(new JointsAngle { Angle = three });
                            Global.list.Add(new JointsAngle { Angle = four });
                            Global.list.Add(new JointsAngle { Angle = five });
                            Global.list.Add(new JointsAngle { Angle = six });
                            Global.list.Add(new JointsAngle { Angle = seven });
                            Global.list.Add(new JointsAngle { Angle = eight });
                            Global.list.Add(new JointsAngle { Angle = nine });
                            Global.list.Add(new JointsAngle { Angle = ten });
                            Global.list.Add(new JointsAngle { Angle = eleven });
                            Global.list.Add(new JointsAngle { Angle = twelve });
                            Global.list.Add(new JointsAngle { Angle = thirteen });
                            Global.list.Add(new JointsAngle { Angle = fourteen });
                        }
                    }

                }
            }
        }

        /* This method returns the angle between joints in space in degrees */
        private static double GetAngle(Tuple<float, float, float> center, Tuple<float, float, float> jointA, Tuple<float, float, float> jointB)
        {
            /* Variables that hold the distance between two points in space */
            double OADist = GetDistance(center, jointA);
            double OBDist = GetDistance(center, jointB);

            /* Variables that hold a tuple of a vector coordinates */
            Tuple<float, float, float> v1 = Tuple.Create(jointA.Item1 - center.Item1, jointA.Item2 - center.Item2, jointA.Item3 - center.Item3);
            Tuple<float, float, float> v2 = Tuple.Create(jointB.Item1 - center.Item1, jointB.Item2 - center.Item2, jointB.Item3 - center.Item3);

            /* Variable that holds the scalar product */
            double scalar = GetScalar(v1, v2);

            double oper = scalar / (OADist * OBDist);

            return Math.Acos(oper) * 180 / Math.PI;
        }

        /* Returns the scalar product */
        private static double GetScalar(Tuple<float, float, float> v1, Tuple<float, float, float> v2)
        {
            return (v1.Item1 * v2.Item1) + (v1.Item2 * v2.Item2) + (v1.Item3 * v2.Item3);
        }

        /* Returns the distance between two points in space */
        private static double GetDistance(Tuple<float, float, float> center, Tuple<float, float, float> joint)
        {
            double xSq = Math.Pow(center.Item1 - joint.Item1, 2);
            double ySq = Math.Pow(center.Item2 - joint.Item2, 2);
            double zSq = Math.Pow(center.Item3 - joint.Item3, 2);

            double sum = xSq + ySq + zSq;

            return Math.Sqrt(sum);
        }
    }
}